# PyOBD v2.0

Fork of PyOBD ported to Python3, with my own tweaks.

Original project: http://www.obdtester.com/

I'll make this pretty later.


*Install*



DEBIAN
sudo apt-get install python-wxtools libgtk-3-dev
pip install serial
pip install wxPython

CENTOS
sudo zypper install python3-wxtools gtk3-devel
pip install serial
pip install wxPython
